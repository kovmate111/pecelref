<?php //Adatb megnyitása

    //ONLY FOR DEMO PURPOSES
    $editor=isset($_GET['ed']);
    
    
    $sql_user = "visitor_php";
    $sql_pwd = "sC2dMDAVxBykxPHRbhZy5fWj";
    if($editor)
    {
        $sql_user = "editor_php";
        $sql_pwd = "gZffWxLs6RFtJ2BAxVK9W7Xu";
    }
    $dblink = mysqli_connect("localhost", $sql_user, $sql_pwd) or die("Adatbázis hiba: ".mysqli_error());
    mysqli_select_db($dblink, "pecelref");
    mysqli_query($dblink, "SET character_set_results = 'utf-8'");
    
    //Menü linkek aktívsága
    $bemutatkozas_menu_link_active="\"nav-link\"";
    $hirek_menu_link_active="\"nav-link\"";
    $igehirdetesek_menu_link_active="\"nav-link\"";
    $mediaanyagok_menu_link_active="\"nav-link\"";
    $lelkipasztor_gondnok_menu_link_active="\"nav-link\"";
    $presbiterium_menu_link_active="\"nav-link\"";
    $rendszeres_menu_link_active="\"nav-link\"";
    $meghivok_menu_link_active="\"nav-link\"";
    $eskuvo_keresztelo_menu_link_active="\"nav-link\"";
    $hitunk_alapjai_menu_link_active="\"nav-link\"";
    $egyhaztagsag_konfirmacio_menu_link_active="\"nav-link\"";
    $raday_menu_link_active="\"nav-link\"";
    $templom_menu_link_active="\"nav-link\"";
    $linktar_menu_link_active="\"nav-link\"";
    $kapcsolat_menu_link_active="\"nav-link\"";
    $iratterjesztes_menu_link_active="\"nav-link\"";
    $galeria_menu_link_active="\"nav-link\"";
    
    $alert404=false;
    
    //Oldal kiválasztása
    set_include_path("./src/views/");
    if(isset($_GET['path']))
    {
        switch($_GET['path'])
        {
            case "bemutatkozas":
            {
                $titlePrefix="Bemutatkozás | ";
                $bemutatkozas_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='bemutatkozas.php';
                break;
            }
            case "hirek":
            {
                $titlePrefix="Hírek | ";
                $hirek_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='hirek.php';
                break;
            }
            case "igehirdetesek":
            {
                $titlePrefix="Igehirdetések | ";
                $igehirdetesek_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='igehirdetesek.php';
                break;
            }
            case "mediaanyagok":
            {
                $titlePrefix="Média Anyagok | ";
                $mediaanyagok_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='mediaanyagok.php';
                break;
            }
            case "lelkipasztor-gondnok":
            {
                $titlePrefix="Lelkipásztorok és Gondnokok | ";
                $lelkipasztor_gondnok_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='lelkipasztor-gondnok.php';
                break;
            }
            case "presbiterium":
            {
                $titlePrefix="Presbitérium 2018-2024 | ";
                $presbiterium_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='presbiterium.php';
                break;
            }
            case "rendszeres":
            {
                $titlePrefix="Rendszeres alkalmaink | ";
                $rendszeres_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='rendszeres.php';
                break;
            }
            case "meghivok":
            {
                $titlePrefix="Meghívók | ";
                $meghivok_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='meghivok.php';
                break;
            }
            case "eskuvo":
            {
                $titlePrefix="Esküvő és Keresztelő | ";
                $eskuvo_keresztelo_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='eskuvo-keresztelo.php';
                break;
            }
            case "hitunk-alapjai":
            {
                $titlePrefix="Hitünk Alapjai | ";
                $hitunk_alapjai_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='hitunk-alapjai.php';
                break;
            }
            case "egyhaztagsag":
            {
                $titlePrefix="Egyháztagság és Konfirmáció | ";
                $egyhaztagsag_konfirmacio_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='egyhaztagsag-konfirmacio.php';
                break;
            }
            case "raday":
            {
                $titlePrefix="Ráday Alapítvány | ";
                $raday_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='raday.php';
                break;
            }
            case "templom":
            {
                $titlePrefix="Templomunk | ";
                $templom_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='templom.php';
                break;
            }
            case "linktar":
            {
                $titlePrefix="Linktár | ";
                $linktar_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='linktar.php';
                break;
            }
            case "kapcsolat":
            {
                $titlePrefix="Kapcsolat | ";
                $kapcsolat_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='kapcsolat.php';
                break;
            }
            case "iratterjesztes":
            {
                $titlePrefix="Iratterjesztés | ";
                $iratterjesztes_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='iratterjesztes.php';
                break;
            }
            case "galeria":
            {
                $titlePrefix="Képgaléria | ";
                $galeria_menu_link_active="\"nav-link active\" aria-current=\"page\"";
                $pageContentURL='galeria.php';
                break;
            }
            default:
            {
                $titlePrefix="";
                $pageContentURL='kezdooldal.php';
                $alert404=true;
            }
        }
    }
    else
    {
        $titlePrefix="";
        $pageContentURL='kezdooldal.php';
    }
?>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="utf-8">
        <meta name="author" content="Kovács Máté">
        <meta name="description" content="Péceli Református Egyházközség (pecelref) hivatalos weboldala. Itt találhatók a gyülekezetben közelgő események, elhangzott igehirdetések felvételei, a gyülekezet tevékenységének és hitünk alapjainak ismertetése, hasznos linkek, kapcsolattartási és megközelíthetőségi információk, és egyéb, a gyülekezettel kapcsolatos, letölthető anyagok.">
        <meta name="keywords" content="pecelref, Péceli, péceli, Pecelref, Református, református, Egyházközség, egyházközség, gyülekezet, Gyülekezet, Pécel, pécel">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" href="assets/css/global.css"> -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <title><?=$titlePrefix?>Péceli Református Egyházközség</title>
    </head>
    <body>
        <nav id="headerbar" class="navbar navbar-expand-xl navbar-dark shadow sticky-top" role="navigation" style="background-color: #058420; width: 100vw">
            <div class="container-fluid">
                <a class="navbar-brand" href="/" title="Kezdőoldal">
                    <img src="/assets/icons/headerIcon.svg" alt="" width="48" height="48" class="d-inline-block align-text-center" />
                    Péceli Református Egyházközség
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/bemutatkozas" class=<?=$bemutatkozas_menu_link_active?>>Bemutatkozás</a>
                        </li>
                        <li class="nav-item">
                            <a href="/hirek" class=<?=$hirek_menu_link_active?>>Hírek</a>
                        </li>
                        <li class="nav-item">
                            <a href="/igehirdetesek" class=<?=$igehirdetesek_menu_link_active?>>Igehirdetések</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">Egyéb média</a>
                            <ul class="dropdown-menu shadow px-2" aria-labelledby="navbarDropdownMenuLink" style="background-color: #cefdd8">
                                <li>
                                    <a href="/iratterjesztes" style="color: #058420" class=<?=$iratterjesztes_menu_link_active?>>Iratterjesztés</a>
                                </li>
                                <li>
                                    <a href="/mediaanyagok" style="color: #058420" class=<?=$mediaanyagok_menu_link_active?>>Média Anyagok</a>
                                </li>
                                <li>
                                    <a href="/galeria" style="color: #058420" class=<?=$galeria_menu_link_active?>>Képgaléria</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Információk
                            </a>
                            <ul class="dropdown-menu shadow px-2" style="background-color: #cefdd8" aria-labelledby="navbarDropdownMenuLink">
                                <li>
                                    <a href="/lelkipasztor-gondnok" style="color: #058420" class=<?=$lelkipasztor_gondnok_menu_link_active?>>Lelkipásztorok és Gondnokok</a>
                                </li>
                                <li>
                                    <a href="/presbiterium" style="color: #058420" class=<?=$presbiterium_menu_link_active?>>Presbitérium 2018-2024</a>
                                </li>
                                <li>
                                    <a href="/rendszeres" style="color: #058420" class=<?=$rendszeres_menu_link_active?>>Rendszeres alkalmaink</a>
                                </li>
                                <li>
                                    <a href="/meghivok" style="color: #058420" class=<?=$meghivok_menu_link_active?>>Meghívók</a>
                                </li>
                                <li>
                                    <a href="/eskuvo" style="color: #058420" class=<?=$eskuvo_keresztelo_menu_link_active?>>Esküvő és Keresztelő</a>
                                </li>
                                <li>
                                    <a href="/hitunk-alapjai" style="color: #058420" class=<?=$hitunk_alapjai_menu_link_active?>>Hitünk alapjai</a>
                                </li>
                                <li>
                                    <a href="/egyhaztagsag" style="color: #058420" class=<?=$egyhaztagsag_konfirmacio_menu_link_active?>>Egyháztagság és Konfirmáció</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="/raday" class=<?=$raday_menu_link_active?>>Ráday</a>
                        </li>
                        <li class="nav-item">
                            <a href="/templom" class=<?=$templom_menu_link_active?>>Templom</a>
                        </li>
                        <li class="nav-item">
                            <a href="/linktar" class=<?=$linktar_menu_link_active?>>Linktár</a>
                        </li>
                        <li class="nav-item">
                            <a href="/kapcsolat" class=<?=$kapcsolat_menu_link_active?>>Kapcsolat</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <?php //Oldal tartalma (banner-el együtt)
            /*if($alert404)
            {
                echo "<div class=\"mx-3 alert alert-warning alert-dismissible fade show\" role=\"alert\"> A(z) ".$_GET['path']." oldal nem létezik. Használja a menüt az oldalon történő navigációhoz.<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\" aria-label=\"Close\"></button></div>";
            }*/
            include ($pageContentURL);
        ?>
        <div id="footerbar" class="container-fluid bg-dark py-2 text-white text-center">
            <span>&copy Copyright Péceli Református Egyházközség, 2021</span>
        <div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    </body>
</html>
