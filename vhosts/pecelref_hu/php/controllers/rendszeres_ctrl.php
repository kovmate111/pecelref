<?php

    function explainRendsz($rendszeressegNum, $hetNap)
    {
        switch($hetNap)
        {
            case 1:
            {
                $napStr = "Hétfőn";
                $napStr2 = "Hétfőjén";
                break;
            }
            case 2:
            {
                $napStr = "Kedden";
                $napStr2 = "Keddjén";
                break;
            }
            case 3:
            {
                $napStr = "Szerdán";
                $napStr2 = "Szerdáján";
                break;
            }
            case 4:
            {
                $napStr = "Csütörtökön";
                $napStr2 = "Csütörtökjén";
                break;
            }
            case 5:
            {
                $napStr = "Pénteken";
                $napStr2 = "Péntekjén";
                break;
            }
            case 6:
            {
                $napStr = "Szombaton";
                $napStr2 = "Szombatján";
                break;
            }
            case 7:
            {
                $napStr = "Vasárnap";
                $napStr2 = "Vasárnapján";
            }
        }
    
        switch($rendszeressegNum)
        {
            case 0:
            {
                return "Minden ".$napStr;
                break;
            }
            case 1:
            {
                return "Hónap első ".$napStr2;
                break;
            }
            case 2:
            {
                return "Minden páros hét ".$napStr2;
                break;
            }
            case -1:
            {
                return "Hónap utolsó ".$napStr2;
                break;
            }
            case -2:
            {
                return "Minden páratlan hét ".$napStr2;
                break;
            }
            default: return "";
        }
    }
    
    $visitorMondayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=1 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorMondayTab = mysqli_query($dblink, $visitorMondayRq);
    
    $visitorTuesdayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=2 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorTuesdayTab = mysqli_query($dblink, $visitorTuesdayRq);
    
    $visitorWednesdayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=3 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorWednesdayTab = mysqli_query($dblink, $visitorWednesdayRq);
    
    $visitorThursdayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=4 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorThursdayTab = mysqli_query($dblink, $visitorThursdayRq);
    
    $visitorFridayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=5 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorFridayTab = mysqli_query($dblink, $visitorFridayRq);
    
    $visitorSaturdayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=6 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorSaturdayTab = mysqli_query($dblink, $visitorSaturdayRq);
    
    $visitorSundayRq = "SELECT hetiAlkalmak.megnev AS megnev, DATE_FORMAT(ido, '%k:%i') AS ido, rendszeresseg, helyszin.megnev AS helysz FROM hetiAlkalmak INNER JOIN helyszin ON hetiAlkalmak.helyszinId = helyszin.id WHERE hetNapja=7 ORDER BY hetiAlkalmak.ido ASC;";
    $visitorSundayTab = mysqli_query($dblink, $visitorSundayRq);
?>
