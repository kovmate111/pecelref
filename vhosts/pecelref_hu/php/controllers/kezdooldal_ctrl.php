<?php
    function shortWeekday($num)
    {
        switch($num)
        {
            case 1:
            {
                $rtn="H";
                break;
            }
            case 2:
            {
                $rtn="K";
                break;
            }
            case 3:
            {
                $rtn="Sze";
                break;
            }
            case 4:
            {
                $rtn="Cs";
                break;
            }
            case 5:
            {
                $rtn="P";
                break;
            }
            case 6:
            {
                $rtn="Szo";
                break;
            }
            case 7:
            {
                $rtn="V";
                break;
            }
        }
        return $rtn;
    }
    function weekdayEng2Hu ($num)
    {
        if($num === 1) return 7; else return $num-1;
    }

    //Hírek előkészítése
    $hirekRq_noStoredProc = "SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum FROM pecelref.hirbejegyzes ORDER BY datum DESC LIMIT 6;";
    $hirekTab = mysqli_query($dblink, $hirekRq_noStoredProc);
    
    //Rendszeres alkalmak előkészítése
    $rendszeresRq_noStoredProc = "SELECT hetiAlkalmak.megnev AS megnev, hetNapja, DATE_FORMAT(ido, '%k:%i') AS ido, helyszin.megnev AS helysz FROM pecelref.hetiAlkalmak INNER JOIN pecelref.helyszin ON hetiAlkalmak.helyszinId=helyszin.id WHERE hetiAlkalmak.rendszeresseg=0 ORDER BY hetNapja ASC, hetiAlkalmak.ido ASC;";
    $alkalmakTab = mysqli_query($dblink, $rendszeresRq_noStoredProc);
?>
