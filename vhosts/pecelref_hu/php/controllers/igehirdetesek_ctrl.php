<?php
    if(isset($_GET['q'])) $_GET['q']=mysqli_real_escape_string($dblink, $_GET['q']);
    if(isset($_GET['d'])) $_GET['d']=mysqli_real_escape_string($dblink, $_GET['d']);
    if(isset($_GET['y'])) $_GET['y']=mysqli_real_escape_string($dblink, $_GET['y']);
    if(isset($_GET['s'])) $_GET['s']=mysqli_real_escape_string($dblink, $_GET['s']);
    if(isset($_GET['cim'])) $_GET['cim']=mysqli_real_escape_string($dblink, $_GET['cim']);
    if(isset($_GET['igehirdeto'])) $_GET['igehirdeto']=mysqli_real_escape_string($dblink, $_GET['igehirdeto']);
    if(isset($_GET['alkalom'])) $_GET['alkalom']=mysqli_real_escape_string($dblink, $_GET['alkalom']);
    
    
    //Bibliai könyv lekérdezés (mind)
    $mindkQuery = "SELECT rovidites FROM pecelref.bibliaiKonyv ORDER BY sorszam;";
    $mindkTab = mysqli_query($dblink, $mindkQuery);
    
    //IH sorozatok lekérdezése
    $sorozatokQuery = sprintf("SELECT igehirdetesSorozat.*, COUNT(igehirdetes.id) AS cnt FROM pecelref.igehirdetesSorozat INNER JOIN pecelref.igehirdetes ON igehirdetesSorozat.id = igehirdetes.igehirdetesSorozatId GROUP BY igehirdetesSorozat.id, igehirdetesSorozat.tema;");
    $sorozatTab = mysqli_query($dblink, $sorozatokQuery);
    
    //Évek lekérdezése
    $yearQuery = "SELECT YEAR(igehirdetes.datum) AS year, COUNT(igehirdetes.id) AS cnt FROM pecelref.igehirdetes GROUP BY YEAR(igehirdetes.datum) ORDER BY YEAR(igehirdetes.datum) DESC;";
    $ihYears = mysqli_query($dblink, $yearQuery);
    
    //TODO csak ó, ill. csak újszövetségi könyvek

    //TODO fejezetszám és versszám validáció JS segítségével
    
    
    //Igehirdetők lekérdezése
    $igehirdetoQuery = sprintf("SELECT szerzo.id, szerzo.nev, COUNT(igehirdetes.id) AS cnt FROM pecelref.szerzo INNER JOIN pecelref.igehirdetes ON szerzo.id = igehirdetes.igehirdetoId GROUP BY szerzo.id, szerzo.nev ORDER BY szerzo.nev ASC;");
    if(isset($_GET['igehirdeto']))
    {
        $igehirdetoResQuery = sprintf("SELECT szerzo.id, szerzo.nev, COUNT(igehirdetes.id) AS cnt FROM pecelref.szerzo INNER JOIN pecelref.igehirdetes ON szerzo.id = igehirdetes.igehirdetoId GROUP BY szerzo.id, szerzo.nev ORDER BY szerzo.nev ASC;");
        if($igehirdetoRes=mysqli_fetch_array(mysqli_query($dblink, $igehirdetoResQuery)))
        {
            $igehirdetoQuery = sprintf("SELECT szerzo.id, szerzo.nev, COUNT(igehirdetes.id) AS cnt FROM pecelref.szerzo INNER JOIN pecelref.igehirdetes ON szerzo.id = igehirdetes.igehirdetoId WHERE NOT szerzo.id=".$_GET['igehirdeto']." GROUP BY szerzo.id, szerzo.nev ORDER BY szerzo.nev ASC;");
        }
    }
    $igehirdetoTab = mysqli_query($dblink, $igehirdetoQuery);
    
    //Alkalmak lekérdezése
    $alkalomQuery = sprintf("SELECT alkalom.id, alkalom.megnev FROM pecelref.alkalom;");
    if(isset($_GET['alkalom']))
    {
        $alkalomResQuery = sprintf("SELECT alkalom.id, alkalom.megnev FROM pecelref.alkalom WHERE id=".$_GET['alkalom'].";");
        if($alkalomRes=mysqli_fetch_array(mysqli_query($dblink, $alkalomResQuery)))
        {
            $alkalomQuery = sprintf("SELECT alkalom.id, alkalom.nev FROM pecelref.alkalom WHERE NOT id=".$_GET['alkalom'].";");
        }
    }
    $alkalomTab = mysqli_query($dblink, $alkalomQuery);
    
    function fejezetek($konyvId)
    {
        $fejezetQuery = "SELECT bibliaiFejezet.id, bibliaiFejezet.sorszam FROM pecelref.bibliaiFejezet WHERE bibliaiKonyvId=".$konyvid." ORDER BY bibliaiFejezet.sorszam ASC;";
        $fejezetTab = mysqli_query($dblink, $fejezetQuery);
        return $fejezetTab;
    }
    function versek($fejezetId)
    {
        $versQuery = "SELECT bibliaiVers.id, BibliaiVers.sorszam FROM pecelref.bibliaiVers WHERE bibliaiFejezetId =".$fejezetId." ORDER BY bibliaiVers.sorszam ASC;";
        $versTab = mysqli_query($dblink, $versQuery);
        return $versTab;
    }
    
    //Tényleges IH-k lekérdezése
    if(isset($_GET['q']) && !($_GET['q']==="")) {$ihQuery = sprintf("SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, szerzo.nev as igehirdetoNev, felhasznalok.username, alkalom.megnev as alkalomNev FROM pecelref.igehirdetes INNER JOIN pecelref.szerzo ON igehirdetes.igehirdetoId=szerzo.id INNER JOIN pecelref.felhasznalok ON igehirdetes.felhasznaloId=felhasznalok.id LEFT OUTER JOIN pecelref.alkalom ON igehirdetes.alkalomId=alkalom.id WHERE cim LIKE '%%%s%%' OR igehirdetoNev LIKE '%%%s%%' OR alkalomNev LIKE '%%%s%%' ORDER BY datum DESC;", $_GET['q'], $_GET['q'], $_GET['q']);} //Egyszerű keresés esetén
    
    else if(isset($_GET['y'])) {$ihQuery = sprintf("SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, szerzo.nev as igehirdetoNev, felhasznalok.username, alkalom.megnev as alkalomNev FROM pecelref.igehirdetes INNER JOIN pecelref.szerzo ON igehirdetes.igehirdetoId=szerzo.id INNER JOIN pecelref.felhasznalok ON igehirdetes.felhasznaloId=felhasznalok.id LEFT OUTER JOIN pecelref.alkalom ON igehirdetes.alkalomId=alkalom.id WHERE YEAR(datum)=".$_GET['y']." ORDER BY datum ASC;");} //Évkiválasztás
    
    else if(isset($_GET['s'])) {$ihQuery = sprintf("SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, szerzo.nev as igehirdetoNev, felhasznalok.username, alkalom.megnev as alkalomNev FROM pecelref.igehirdetes INNER JOIN pecelref.szerzo ON igehirdetes.igehirdetoId=szerzo.id INNER JOIN pecelref.felhasznalok ON igehirdetes.felhasznaloId=felhasznalok.id LEFT OUTER JOIN pecelref.alkalom ON igehirdetes.alkalomId=alkalom.id WHERE igehirdetes.igehirdetesSorozatId=".$_GET['s']." ORDER BY datum ASC;");} //Sorozat listázás
    
    else if(isset($_GET['detail'])) {$ihQuery = sprintf("SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, szerzo.nev as igehirdetoNev, felhasznalok.username, alkalom.megnev as alkalomNev FROM pecelref.igehirdetes INNER JOIN pecelref.szerzo ON igehirdetes.igehirdetoId=szerzo.id INNER JOIN pecelref.felhasznalok ON igehirdetes.felhasznaloId=felhasznalok.id LEFT OUTER JOIN pecelref.alkalom ON igehirdetes.alkalomId=alkalom.id WHERE ".(($_GET['cim'] === "") ? ("TRUE") : ("cim LIKE '%%".$_GET['cim']."%%'"))." AND ".(($_GET['igehirdeto'] === "") ? ("TRUE") : ("igehirdetes.igehirdetoId=".$_GET['igehirdeto']))." AND ".(($_GET['d'] === "") ? ("TRUE") : ("WEEK(igehirdetes.datum)=WEEK(\"".$_GET['d']."\")"))." AND ".(($_GET['alkalom'] === "") ? ("TRUE") : ("igehirdetes.alkalomId=".$_GET['alkalom']))." ORDER BY datum DESC;");} //Összetett kereséssel
    
    else {$ihQuery = sprintf("SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, szerzo.nev as igehirdetoNev, felhasznalok.username, alkalom.megnev as alkalomNev FROM pecelref.igehirdetes INNER JOIN pecelref.szerzo ON igehirdetes.igehirdetoId=szerzo.id INNER JOIN pecelref.felhasznalok ON igehirdetes.felhasznaloId=felhasznalok.id LEFT OUTER JOIN pecelref.alkalom ON igehirdetes.alkalomId=alkalom.id ORDER BY datum DESC LIMIT 20;");} //Nincs keresés: legutóbbi 20 megjelenítése
    
    $ihTable = mysqli_query($dblink, $ihQuery);
?>
