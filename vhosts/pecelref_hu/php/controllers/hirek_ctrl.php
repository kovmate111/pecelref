<?php
    $_GET['q']=mysqli_real_escape_string($dblink, $_GET['q']);
    $_GET['d']=mysqli_real_escape_string($dblink, $_GET['d']);
    $_GET['y']=mysqli_real_escape_string($dblink, $_GET['y']);
    //Hír szűrés előállítása ha van GET
    if(isset($_GET['q']) && (!isset($_GET['d']) || $_GET['d'] === ""))
    {
        $hirekQuery= sprintf("SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username FROM pecelref.hirbejegyzes INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id WHERE cim LIKE '%%%s%%' OR szoveg LIKE '%%%s%%' ORDER BY hirbejegyzes.datum DESC;", mysqli_real_escape_string($dblink, $_GET['q']), mysqli_real_escape_string($dblink, $_GET['q']));
    }
    else if(isset($_GET['d']) && (!isset($_GET['q']) || $_GET['q'] === ""))
    {
        $hirekQuery=sprintf("SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username FROM pecelref.hirbejegyzes INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id WHERE MONTH(datum) = MONTH(\"%s\") ORDER BY hirbejegyzes.datum DESC;", $_GET['d']);
    }
    else if(isset($_GET['q']) && isset($_GET['d']) && !($_GET['d']==="") && !($_GET['q']===""))
    {
        $hirekQuery=sprintf("SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username FROM pecelref.hirbejegyzes INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id WHERE MONTH(datum) = MONTH(\"%s\") AND (cim LIKE '%%%s%%' OR szoveg LIKE '%%%s%%') ORDER BY hirbejegyzes.datum DESC;", $_GET['d'], mysqli_real_escape_string($dblink, $_GET['q']), mysqli_real_escape_string($dblink, $_GET['q']));
    }
    else if(isset($_GET['y']))
    {
        $hirekQuery="SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username FROM pecelref.hirbejegyzes INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id WHERE YEAR(datum)=".$_GET['y']." ORDER BY hirbejegyzes.datum DESC;";
    }
    else
    {
        $hirekQuery="SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username FROM pecelref.hirbejegyzes INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id WHERE DATEDIFF(curdate(), datum) < 365 ORDER BY hirbejegyzes.datum DESC;";
    }
    $hirbTable = mysqli_query($dblink, $hirekQuery);
    
    $yearQuery = "SELECT YEAR(hirbejegyzes.datum) AS year, COUNT(hirbejegyzes.id) AS cnt FROM pecelref.hirbejegyzes GROUp BY YEAR(hirbejegyzes.datum) ORDER BY YEAR(hirbejegyzes.datum) DESC;";
    $newsYears = mysqli_query($dblink, $yearQuery);
?>
