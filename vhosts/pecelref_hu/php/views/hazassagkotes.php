        <div id="contentArea">
                <p>
                    Isten Igéje szerint a házasság egy nő és egy férfi életre szóló szövetsége. Mindketten felelősek a döntésükért, amellyel felvállalják a másikat, megosztják vele életüket, örömeiket és bánataikat. A Teremtő Isten egész életre szóló kapcsolatként tervezte a házasságot, ezért segítségét ajánlja fel mindazoknak, akik erre hűségesen kérik őt, hogy e kapcsolat örömet, formálódást és áldást jelentsen mindkét félnek. Gyülekezetünk imádságos szívvel és szeretettel fogadja a házasságra készülőket.
                </p>
                <p>
                    Szeretettel kérjük, hogy mindketten látogassanak el egyik istentiszteletünkre (vasárnap délelőtt 10 óra). Ez lehetőség arra, hogy megtekintsék templomunkat és a református istentisztelet menetét, találkozzanak lelkészeinkkel és megismerkedjenek a gyülekezettel.
                </p>
                <p>
                    Az esküvőre bejelentkezni személyesen lehet a Lelkészi Hivatalban. Az esküvőre készülő párok néhány alkalmas esküvői    felkészítésen vesznek részt az eskető lelkésszel. Az esküvőt megelőző vasárnap, a délelőtti istentiszteleten név szerint kihirdetjük a házasulandókat. Kérjük, erre az alkalomra mindenképpen jöjjenek el.
                </p>
        </div>
