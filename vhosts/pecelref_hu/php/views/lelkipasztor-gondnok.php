        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/lelkipasztor-gondnok.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Lelkipásztorok és Gondnokok</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <h3>Gyülekezetünk lelkipásztorai</h3>
                <ul>
                    <li><b>Vecsei Csász János</b> 1695 előtt - 1719</li>
                    <li><b>Patay András</b> 1719 - 1732</li>
                    <li><b>Vég Veresmarti Sámuel</b> 1732 - 1749</li>
                    <li><b>Fejes István</b> 1749 - 1760</li>
                    <li><b>Dobray István</b> 1760 - 1764</li>
                    <li>...</li>
                </ul>
                <h3>Gyülekezetünk főgondnokai</h3>
                <ul>
                    <li><b>Fáy ANdrás</b> 1809 - 1811</li>
                    <li>...</li>
                </ul>
                <h3>Gyülekezetünk gondnokai (1974-től)</h3>
                <ul>
                    <li><b>Papp József</b> 1974 - 1979</li>
                    <li><b>Rigó János</b> 1980 - 1983</li>
                    <li>...</li>
                </ul>
            </div>
        </div>
