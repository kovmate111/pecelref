<?php
    require "./src/controllers/igehirdetesek_ctrl.php";
?>
        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/igehirdetesek.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Igehirdetések</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <div id="searchbox" class="card mb-4 shadow" style="background-color: #cefdd8">
                    <div class="card-body">
                        <div class="row gy-3 align-items-center">
                            <div class="col-md-2">
                                <h5 class="card-title fs-4">Keresés</h5>
                            </div>
                            <div class="col-md-8">
                                <div id="simpleForm" class=<?=isset($_GET['detail']) ? "\"collapse multi-collapse\"" : "\"collapse multi-collapse show\""?>>
                                    <form method="get" action="/igehirdetesek">
                                        <div class="input-group">
                                            <label for="searchField" class="visually-hidden">Egyszerű szöveges keresés</label>
                                            <input type="text" class="form-control rounded-start border border-3 border-success" style="border-color: #058420" placeholder="Szöveges keresés" id="txtSearch" name="q" value=<?="\"".isset($_GET['q']) ? mysqli_real_escape_string($dblink, $_GET['q']) : ""."\""?>>
                                            <button type="submit" class="btn btn-primary rounded-end me-3" style="background-color: #058420" title="Gyors keresés cím és/vagy dátum alapján">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                                </svg>
                                                Keresés
                                            </button>
                                            <button class="btn btn-outline-danger rounded" type="reset" title="Keresési feltételek alaphelyzetbe állítása">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-backspace" viewBox="0 0 16 16">
                                                    <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"/>
                                                    <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"/>
                                                </svg>
                                                Alaphelyzet
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-outline-secondary" type="button" data-bs-toggle="collapse" data-bs-target=".multi-collapse" aria-controls="simpleForm detailedForm" title="Váltás egyszerű és részletes keresés között" aria-expanded=<?=isset($_GET['detail']) ? "\"true\"" : "\"false\""?>>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-funnel" viewBox="0 0 16 16">
                                        <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2zm1 .5v1.308l4.372 4.858A.5.5 0 0 1 7 8.5v5.306l2-.666V8.5a.5.5 0 0 1 .128-.334L13.5 3.308V2h-11z"/>
                                    </svg>
                                    Részletes keresés
                                </button>
                            </div>
                        </div>
                        <div id="detailedForm" class=<?=isset($_GET['detail']) ? "\"collapse multi-collapse mx-md-4 show\"" : "\"collapse multi-collapse mx-md-4\""?>>
                            <form method="get" action="/igehirdetesek?detail=true">
                                <div class="row gy-3 my-1 align-items-center">
                                    <label for="titleField" class="col-form-label col-sm-2 text-end">Cím:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="titleField" name="cim" value=<?="\"".isset($_GET['cim']) ? mysqli_real_escape_string($dblink, $_GET['cim']) : "" ."\""?>>
                                    </div>
                                    <label for="igehirdetoDropdn" class="col-form-label col-sm-2 text-end">Igehirdető:</label>
                                    <div class="col-sm-4">
                                        <select class="form-select" id="igehirdetoDropdn" name="igehirdeto">
                                            <?php if(isset($igehirdetoRes)): ?>
                                            <option selected value=<?="\"".$igehirdetoRes['id']."\""?>>
                                                <?=$igehirdetoRes['nev']?>
                                            </option>
                                            <?php else: ?>
                                            <option selected> </option>
                                            <?php endif; ?>
                                            <?php while($igehirdetoRow=mysqli_fetch_array($igehirdetoTab)): ?>
                                            <option value=<?="\"".$igehirdetoRow['id']."\""?>><?=$igehirdetoRow['nev']?></option>
                                            <?php endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row gy-3 my-1 align-items-center">
                                    <label for="dateField" class="col-form-label col-sm-2 text-end" title="Nem kell pontosan megadni, csak a hét kell egyezzen">Dátum:</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="dateField" name="d" class="form-control" value=<?=isset($_GET['d']) ? "\"".mysqli_real_escape_string($dblink, $_GET['d'])."\"" : ""?>>
                                    </div>
                                    <label for="alkalomDropdn" class="col-form-label col-sm-2 text-end">Alkalom:</label>
                                    <div class="col-sm-4">
                                        <select class="form-select" id="alkalomDropdn" name="alkalom">
                                            <?php if(isset($alkalomRes)): ?>
                                            <option selected value=<?="\"".$alkalomRes['id']."\""?>>
                                                <?=$alkalomRes['megnev']?>
                                            </option>
                                            <?php else: ?>
                                            <option selected> </option>
                                            <?php endif; ?>
                                            <?php while($alkalomRow=mysqli_fetch_array($alkalomTab)): ?>
                                            <option value=<?="\"".$alkalomRow['id']."\""?>><?=$alkalomRow['megnev']?></option>
                                            <?php endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row gy-3 my-1 align-items-center">
                                    <div class="col">Az igehely-alapú keresés még fejlesztés alatt áll.</div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary" style="background-color: #058420" title="Részletes keresés" name="detail">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                            </svg>
                                            Keresés
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-outline-danger" type="reset" title="Keresési feltételek alaphelyzetbe állítása">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-backspace" viewBox="0 0 16 16">
                                                <path d="M5.83 5.146a.5.5 0 0 0 0 .708L7.975 8l-2.147 2.146a.5.5 0 0 0 .707.708l2.147-2.147 2.146 2.147a.5.5 0 0 0 .707-.708L9.39 8l2.146-2.146a.5.5 0 0 0-.707-.708L8.683 7.293 6.536 5.146a.5.5 0 0 0-.707 0z"/>
                                                <path d="M13.683 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-7.08a2 2 0 0 1-1.519-.698L.241 8.65a1 1 0 0 1 0-1.302L5.084 1.7A2 2 0 0 1 6.603 1h7.08zm-7.08 1a1 1 0 0 0-.76.35L1 8l4.844 5.65a1 1 0 0 0 .759.35h7.08a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1h-7.08z"/>
                                            </svg>
                                            Alaphelyzet
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9"><!-- maguk az igehirdetések -->
                        <?php if($ihRow=mysqli_fetch_array($ihTable)): ?>
                        <div class="card mb-3" <?="id=\"ih_".$ihRow['id']."\""?>>
                            <div class="card-body">
                                <h5 class="card-title"><?=$ihRow['cim']?></h5>
                                <h6 class="card-subtitle mb-1 text-muted"><?=$ihRow['datum']?></h6>
                                <p class="card-text">Igehirdető: <?=$ihRow['igehirdetoNev']?>; Alkalom: <?=$ihRow['alkalomNev']?><br>Textus: <?=$ihRow['TextusOlvashato']?></p>
                                <div class="row align-items-center">
                                    <?php if(isset($ihRow['youtubeURL'])): ?>
                                    <div class="col-3">
                                        <button class="btn btn-primary" type="button" style="background-color: #058420" data-bs-toggle="collapse" data-bs-target=<?="\"videoFrame".$ihRow['id']."\""?> type="button" aria-expanded="false" aria-controls=<?="\"videoFrame".$ihRow['id']."\""?>>YouTube-videó megtekintése</button>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(isset($ihRow['hangURL'])): ?>
                                    <div class="col-3">
                                        <a class="btn btn-primary" style="background-color: #058420" href=<?=$ihRow['hangURL']?>>Hanganyag letöltése</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php if(isset($ihRow['youtubeURL'])): ?>
                                <div class="collapse ratio ratio-16x9" id=<?="\"videoFrame".$ihRow['id']."\""?>>
                                    <iframe title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen src=<?="\"".ihRow['youtubeURL']."\""?>></iframe>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="card-footer">
                                <div class="col">
                                    <p class="text-muted text-end">Feltöltő: <?=$ihRow['username']?></p>
                                </div>
                            </div>
                        </div>
                        <?php else: echo"<div class=\"alert alert-warning\" role=\"alert\">Nincs a keresésnek megfelelő igehirdetés.</div>"; endif; ?>
                        <?php while($ihRow=mysqli_fetch_array($ihTable)): ?>
                        <div class="card mb-3" <?="id=\"ih_".$ihRow['id']."\""?>>
                            <div class="card-body">
                                <h5 class="card-title"><?=$ihRow['cim']?></h5>
                                <h6 class="card-subtitle mb-1 text-muted"><?=$ihRow['datum']?></h6>
                                <p class="card-text">Igehirdető: <?=$ihRow['igehirdetoNev']?>; Alkalom: <?=$ihRow['alkalomNev']?><br>Textus: <?=$ihRow['TextusOlvashato']?></p>
                                <div class="row align-items-center">
                                    <?php if(isset($ihRow['youtubeURL'])): ?>
                                    <div class="col-3">
                                        <button class="btn btn-primary" type="button" style="background-color: #058420" data-bs-toggle="collapse" data-bs-target=<?="\"videoFrame".$ihRow['id']."\""?> type="button" aria-expanded="false" aria-controls=<?="\"videoFrame".$ihRow['id']."\""?>>YouTube-videó megtekintése</button>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(isset($ihRow['hangURL'])): ?>
                                    <div class="col-3">
                                        <a class="btn btn-primary" style="background-color: #058420" href=<?=$ihRow['hangURL']?>>Hanganyag letöltése</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php if(isset($ihRow['youtubeURL'])): ?>
                                <div class="collapse ratio ratio-16x9" id=<?="\"videoFrame".$ihRow['id']."\""?>>
                                    <iframe title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen src=<?="\"".ihRow['youtubeURL']."\""?>></iframe>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="card-footer">
                                <div class="col">
                                    <p class="text-muted text-end">Feltöltő: <?=$ihRow['username']?></p>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-light shadow mb-3"><!-- sorozatok listázása -->
                            <h5 class="card-header fs-4" style="color: #058420">Sorozatok</h5>
                            <ul class="list-group list-group-flush bg-light">
                                <?php while($sorozatRow=mysqli_fetch_array($sorozatTab)): ?>
                                <li class="list-group-item bg-light"><a href=<?="\"/igehirdetesek?s=".$sorozatRow['id']."\""?>><?=$sorozatRow['tema']?></a> <span class="badge bg-secondary"><?=$sorozatRow['cnt']?></span><span class="visually-hidden">A <?=$sorozatRow['tema']?> sorozatba tartozó igehirdetések száma</span></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                        <div class="card bg-light mb-3 shadow"><!-- évszám választó -->
                            <h5 class="card-header fs-4" style="color: #058420">Év szerint</h5>
                            <ul class="list-group list-group-flush bg-light">
                                <?php while($yearRow=mysqli_fetch_array($ihYears)): ?>
                                <li class="list-group-item bg-light"><a href=<?="\"/igehirdetesek?y=".$yearRow['year']."\""?>><?=$yearRow['year']?></a> <span class="badge bg-secondary"><?=$yearRow['cnt']?></span><span class="visually-hidden">A <?=$yearRow['year']?>. évben elhangzott igehirdetések száma</span></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
