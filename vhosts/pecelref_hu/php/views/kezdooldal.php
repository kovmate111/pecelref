<?php
    require "./src/controllers/kezdooldal_ctrl.php";
?>
        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/home.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Péceli Református Egyházközség</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="contentArea" class="col-md-9">
                    <p class="fs-6 mb-3">Jézust megismerni és benne megmaradni: ez az élet. A gyülekezet lelki otthon, nekünk, pécelieknek is. A kicsiknek és a nagyoknak is.<br>
                    Nem a hobbink tart össze bennünket, nem a kisállatkáink, nem a szokásaink. Alapunk a meg-nem-ingó talaj: Ő Jézus, aki összetart minket, Őmiatta találkozunk hétről hétre, és Őmiatta szeretjük egymást.<br>
                    A gyülekezetünk egy hely, ahova érdemes jönni, ha az ember szomorú, vagy ha örül, ha nem tud mit kezdeni magával, vagy ha a sok elfoglaltság között mégis itt akar lenni, ha hallani akar Istentől, ha szolgálni akar, ha építeni akar lelkeket, vagy ha úgy érzi, lelkileg összeroppant és épülésre van szüksége.<br>
                    Érdemes jönni csillogó szemű gyermekként, érdemes jönni lázadó fiatalként, érdemes jönni akkor is, ha nagy tempót diktál az élet és kicsit megfáradt felnőttként, érdemes jönni annak is, aki már nem érzi magát fiatalnak, érdemes jönni nagyon hívőként és nem nagyon hívőként.<br>
                    Érdemes jönni, de nem egymásért, hanem Jézusért.</p>
                    <div id="hirek">
                        <h3 class="text-center">Hírek</h3>
                        <?php
                            while($hirekRow = mysqli_fetch_array($hirekTab)): 
                        ?>
                        <div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title"><?=$hirekRow['cim']?></h5>
                                <h6 class="card-subtitle mb-1 text-muted"><?=$hirekRow['datum']?></h6>
                                <div class="card-text text-truncate text-wrap" style="max-height: 4.5rem !important; text-overflow: ellipsis"><?=$hirekRow['szoveg']?></div><?php echo "<a href=\"/hirek#article_".$hirekRow['id']."\" class=\"card-link\" style=\"color: #058420\">Részletek</a>";?>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <div class="d-grid col-3 mx-auto mb-3">
                            <a href="/hirek" class="btn btn-primary" style="background-color: #058420">Több hír</a>
                        </div>
                    </div>
                </div>
                <div id="rightSidebar" class="col-md-3">
                    <div id="kapcsolatMini" class="card bg-light mb-3 shadow">
                        <a href="/kapcsolat" style="color: #058420"><h3 class="card-header fs-4">Kapcsolat</h3></a>
                        <div class="card-body bg-light">
                            <p class="card-text">Péceli Református Egyházközség<br>2119 Pécel, Kálvin Tér 2.<br>Tel./Fax: 06-28-742-498<br>E-mail: pecelref@gmail.com</p>
                            <p class="card-text">Lelkészi hivatal nyitva tartása:<br>Hétfő: 9-12, 16-18<br>Kedd: 9-12<br>Szerda: szünnap<br>Csütörtök: 9-12, 16-18<br>Péntek: 9-12</p>
                        </div>
                    </div>
                    <div id="rendszeresMini" class="card bg-light mb-3 shadow">
                        <a href="/rendszeres" style="color: #058420"><h3 class="card-header fs-4">Rendszeres alkalmaink</h3></a>
                        <ul class="list-group list-group-flush bg-light">
                            <?php
                                while($rendszRow = mysqli_fetch_array($alkalmakTab)): 
                            ?>
                            <li class="list-group-item bg-light">
                                <b><?=shortWeekday($rendszRow['hetNapja'])?> <?=$rendszRow['ido']?>:</b> <?=$rendszRow['megnev']?> (<?=$rendszRow['helysz']?>)
                            </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
