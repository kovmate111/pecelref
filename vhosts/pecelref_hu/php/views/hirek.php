<?php
    require "./src/controllers/hirek_ctrl.php";
?>
        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/hirek.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Hírek</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <div id="searchbox" class="card mb-4 shadow" style="background-color: #cefdd8">
                    <div class="card-body">
                        <form class="row gy-3 align-items-center" method="get" action="/hirek">
                            <div class="col-3">
                                <h5 class="card-title fs-4">Keresés</h5>
                            </div>
                            <div class="col-md-4">
                                <label for="searchField" class="visually-hidden">Keresés cím vagy tartalom alapján</label>
                                <input type="text" class="form-control border border-3 border-success" placeholder="Keresés cím vagy tartalom alapján" id="searchField" name="q" style="background-color: #e7feec" value=<?="\"".mysqli_real_escape_string($dblink, $_GET['q'])."\""?>>
                            </div>
                            <div class="col-md-2">
                                <label for="dateField" class="visually-hidden">Keresés dátum alapján</label>
                                <input type="date" id="dateField" name="d" class="form-control border border-3 border-success" style="background-color: #e7feec">
                            </div>
                            <div class="col-3">
                                <button type="submit" class="btn btn-primary" style="background-color: #058420" title="Keresés a hírek között a megadott feltételekkel">Keresés</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9"><!-- maguk a hírek -->
                        <?php if($bejegyz=mysqli_fetch_array($hirbTable)): ?>
                        <div class="card mb-3" <?="id=\"article_".$bejegyz['id']."\""?>>
                            <div class="card-body">
                                <h5 class="card-title"><?=$bejegyz['cim']?></h5>
                                <h6 class="card-subtitle mb-1 text-muted"><?=$bejegyz['datum']?></h6>
                                <p class="card-text"><?=$bejegyz['szoveg']?></p>
                            </div>
                            <div class="card-footer text-muted text-end">
                                <p>Feltöltő: <?=$bejegyz['username']?></p>
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="alert alert-warning" role="alert">Nincs a keresésnek megfelelő bejegyzés.</div>
                        <?php endif; ?>
                        <?php while($bejegyz=mysqli_fetch_array($hirbTable)): ?>
                        <div class="card mb-3" <?="id=\"article_".$bejegyz['id']."\""?>>
                            <div class="card-body">
                                <h5 class="card-title"><?=$bejegyz['cim']?></h5>
                                <h6 class="card-subtitle mb-1 text-muted"><?=$bejegyz['datum']?></h6>
                                <p class="card-text"><?=$bejegyz['szoveg']?></p>
                            </div>
                            <div class="card-footer text-muted text-end">
                                <p>Feltöltő: <?=$bejegyz['username']?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="col-md-3"><!-- évszám választó -->
                        <div class="card bg-light shadow">
                            <h5 class="card-header fs-4" style="color: #058420">Archívum</h5>
                            <ul class="list-group list-group-flush bg-light">
                                <?php while($yearRow=mysqli_fetch_array($newsYears)): ?>
                                <li class="list-group-item bg-light"><a href=<?="\"/hirek?y=".$yearRow['year']."\""?>><?=$yearRow['year']?></a> <span class="badge bg-secondary"><?=$yearRow['cnt']?></span><span class="visually-hidden"><?=$yearRow['year']." bejegyzéseinek száma"?></span></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
