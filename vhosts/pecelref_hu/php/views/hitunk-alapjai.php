        <div id="contentArea">
            <h2>  1. Mi jellemzi a református istentiszteletet? </h2>
                <p>
                    A templomainkat a puritán egyszerűség, tisztaság jellemzi, a szobroktól és képektől mentes, általában fehérre meszelt falak dominálnak. A templomainkban csak a legszükségesebbek találhatók: a padok, szószék, úrasztala. A liturgiában a prédikáció, Isten élő Igéje a meghatározó és nem a külsőség.
                </p>

            <h2>  2. Mi jellemzi a református embert? </h2>
                <p>
                    A református hívők életét az egyszerűség, a szorgosság, a munka becsülete, a józan életvitel jellemzi. Az élő hit a bibliaolvasásban, az imádkozás gyakorlásában és az aktív közösségi részvételben nyilvánul meg.
                </p>

            <h2>  3. Mik a református hit alapjai? </h2>
                <ul>
                    <li> A Szentírás </li>
                    <li> Az Apostoli Hitvallás </li>
                    <li> A Heidelbergi Káté </li>
                    <li> A II. Helvét Hitvallás </li>
                </ul>
                <h3> Sola Scriptura - Egyedül a Szentírás </h3>
                    <p>
                        A reformáció visszatért a Biblia eredeti tanításához: Isten hitünk és életünk egyetlen mértékéül a teljes Szentírást rendelte. A reformáció kora óta mindig akkor következett megújulás gyülekezeteink életében, amikor a családokban és a gyülekezetek életében újra fontos lett a teljes Szentírás.
                    </p>

                <h3> Sola Gratia - Egyedül kegyelemből </h3>
                    <p>
                        A reformátusok hiszik, hogy az üdvösség nem emberi teljesítménytől, hanem Isten kegyelmétől függ. Valljuk, hogy Isten minket egyedül Krisztus kereszthaláláért mentett fel a halálos ítélet alól, ezt semmivel sem tudjuk kiérdemelni, mert ezt kegyelemből kapjuk ajándékként.
                    </p>

                <h3> Sola Fide - Egyedül hit által </h3>
                    <p>
                       A megigazulást kizárólag hit által nyerhetjük el, bármilyen jó cselekedet szükségessége és hozzáadása nélkül. A hit nem egy emberi reakció, hanem Isten munkája a kegyelem által.
                    </p>

                <h3> Solus Christus - Egyedül Jézus Krisztus által </h3>
                    <p>
                       Megváltásunk egyedül és kizárólag üdvözítő Jézus Krisztusunk munkája. Jézus Krisztus az egyetlen közbenjáró Isten és ember között.
                    </p>

                <h3> Soli Deo Gloria - Egyedül Istené a dicsőség </h3>
                    <p>
                       Minden dicsőség egyedül Istennek jár, mivel a megváltás kizárólag az Ő akarata és tette által ment végbe: nemcsak Jézus mindenre elégséges bűnhődése a mi bűneinkért a kereszten az Ő munkája, hanem az is, ha mi hiszünk Jézusnak ebben a tettében, és ezt a hitet is az ember szívében a Szentlélek hozza létre.
                    </p>

                <h3> Szentségek, sákramentumok </h3>
                    <p>
                      A Református Egyházunknak a Biblia alapján, Jézus Krisztus parancsa szerint két sákramentuma van: a keresztség és az úrvacsora. A keresztségben egyszerre fogadjuk el Isten ajándékát és fogadunk neki hálából örök hűséget. A keresztség részünkről új kezdést is hirdet: Mostantól kezdve Isten népének tagjaként, Jézus Krisztussal való szövetségben akarok élni. A gyermek helyett a szülei tesznek vallást, és a szülei tesznek fogadalmat arra, hogy gyermeküket a keresztyén egyház közösségében, hitben nevelik. A gyermek helyett – most még – a szülők mutatják meg, hogy a gyülekezet közösségéhez kívánnak tartozni. Jézus Krisztus bocsánatának, kegyelmének ígérete a gyermekeké is. Az úrvacsora emlékezés Jézus Krisztus keresztáldozatára. A bor Krisztus vérét, a kenyér pedig testét jelképezi.
                    </p>


        </div>
