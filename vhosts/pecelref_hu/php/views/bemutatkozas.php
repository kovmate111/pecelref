        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/bemutatkozas.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Bemutatkozás</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <p class="fs-5">Jézust megismerni és benne megmaradni: ez az élet. A gyülekezet lelki otthon, nekünk, pécelieknek is. A kicsiknek és a nagyoknak is.<br>
                Nem a hobbink tart össze bennünket, nem a kisállatkáink, nem a szokásaink. Alapunk a meg-nem-ingó talaj: Ő Jézus, aki összetart minket, Őmiatta találkozunk hétről hétre, és Őmiatta szeretjük egymást.<br>
                A gyülekezetünk egy hely, ahova érdemes jönni, ha az ember szomorú, vagy ha örül, ha nem tud mit kezdeni magával, vagy ha a sok elfoglaltság között mégis itt akar lenni, ha hallani akar Istentől, ha szolgálni akar, ha építeni akar lelkeket, vagy ha úgy érzi, lelkileg összeroppant és épülésre van szüksége.<br>
                Érdemes jönni csillogó szemű gyermekként, érdemes jönni lázadó fiatalként, érdemes jönni akkor is, ha nagy tempót diktál az élet és kicsit megfáradt felnőttként, érdemes jönni annak is, aki már nem érzi magát fiatalnak, érdemes jönni nagyon hívőként és nem nagyon hívőként.<br>
                Érdemes jönni, de nem egymásért, hanem Jézusért.</p>
            </div>
        </div>
