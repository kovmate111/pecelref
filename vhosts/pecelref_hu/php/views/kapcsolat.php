        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/kapcsolat.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Kapcsolat</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <div class="card h-100">
                            <h4 class="card-header">A lelkészi hivatal elérhetőségei</h4>
                            <div class="card-body">
                                <p class="card-text text-center">Péceli Református Egyházközség<br>2119 Pécel, Kálvin Tér 2.<br>Telefon/Fax: 06-28-742-498<br>E-mail: pecelref@gmail.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <h4 class="card-header">A lelkészi hivatal nyitvatartása</h4>
                            <div class="card-body">
                                <p class="card-text text-center">Hétfő: 9-12, 16-18<br>Kedd: 9-12<br>Szerda: szünnap<br>Csütörtök: 9-12, 16-18<br>Péntek: 9-12</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="card">
                            <h4 class="card-header">Megközelíthetőség</h4>
                            <div class="card-body">
                                <p class="card-text"><b>Autóval:</b> Budapest felől, a Péceli úton beérkezve a városba, a körforgalomnál az első kijáratot választva, a Maglódi úton, a református templom utáni első balra fordulásnál.</p>
                                <p class="card-text"><b>Tömegközlekedéssel:</b> Az Örs Vezér teréről induló 169E járattal kell menni a Kossuth tér megállóig, ami a templommal szemben található. A járat érinti a Vasútállomást is. A Budapest-Keleti PU. és Hatvan, ill. Budapest-Keleti PU. és Gödöllő között közlekedő S80-as személyvonatok megállnak Pécelen. </p>
                                <p class="card-text"><b>Térkép:</b></p>
                                <iframe loading="lazy" style="border: 0;" tabindex="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2696.129657107457!2d19.339881965503764!3d47.487387479176896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1fa8d01fd9246d29!2zUMOpY2VsaSBSZWZvcm3DoXR1cyBFZ3low6F6a8O2enPDqWc!5e0!3m2!1shu!2shu!4v1584811379082!5m2!1shu!2shu" allowfullscreen="allowfullscreen" aria-hidden="false" width="600" height="450" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <h4 class="card-header">Technikai információk</h4>
                            <div class="card-body">
                                <p class="card-text"><b>Bankszámlaszám:</b> 10918001-00000061-89060000. (Unicredit Bank)<br><b>Adó 1% technikai szám:</b> 0066</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

