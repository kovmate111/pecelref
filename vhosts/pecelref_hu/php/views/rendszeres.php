<?php
    require "./src/controllers/rendszeres_ctrl.php";
?>
        <div class="container-xxl my-3">
            <div id="banner" class="bg-dark text-white px-3 pt-2 pb-1 mb-4 rounded-2" style="background: url(./assets/images/banner/rendszeres.png); background-size: cover; background-repeat: no-repeat">
                <div class="container-fluid">
                    <h1>Rendszeres alkalmaink</h1>
                    <div class="d-flex justify-content-end">
                        <p class="text-end fs-5" style="max-width: 350px"><q class="fst-italic">De ő ezt mondta nekem: 'Elég neked az én kegyelmem, mert az én erőm erőtlenség által ér célhoz.' Legszívesebben tehát az erőtlenségeimmel dicsekszem, hogy a Krisztus ereje lakozzék bennem.</q><br>2 Kor 12,9</p>
                    </div>
                </div>
            </div>
            <div id="contentArea">
                <div class="card mb-3">
                    <h4 class="card-header">Hétfő</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorMondayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 1)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Kedd</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorTuesdayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 2)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Szerda</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorWednesdayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 3)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Csütörtök</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorThursdayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 4)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Péntek</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorFridayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 5)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Szombat</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorSaturdayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 6)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="card mb-3">
                    <h4 class="card-header">Vasárnap</h4>
                    <?php //TODO editor stuff ?>
                    <ul class="list-group list-group-flush">
                        <?php while($row=mysqli_fetch_array($visitorSundayTab)): ?>
                        <li class="list-group-item"><strong><?=$row['ido']?></strong> <?=$row['megnev']?> (<?=$row['helysz']?>) (<?=explainRendsz($row['rendszeresseg'], 7)?>)</li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
