        <div id="contentArea">
                <p>
                    Egyházunk törvénye – minőségét tekintve – kétféle egyháztagságot különböztet meg.
                </p>
            <h2> <strong> Általános egyháztagság </strong></h2>
                <p>
                    A Magyarországi Református Egyházban mindenki egyháztagnak számít, akit a református egyház szertartása szerint megkereszteltek, valamint az a nagykorú keresztyén, aki magát reformátusnak vallja. Az egyháztagság ténylegesen akkor valósul meg, amikor valaki valamely konkrét gyülekezethez (egyházközséghez) csatlakozik, és annak tagjává lesz.
                </p>
            <h2>
                   <strong> Teljes jogú </strong> (= választói joggal rendelkező)  <strong> egyháztagság </strong>
            </h2>
                <p>
                    Teljes jogú egyháztag az, akit megkereszteltek, konfirmációi fogadalmat tett, a gyülekezet istentiszteletein és az úrvacsorai közösségben részt vesz, és gyülekezete fenntartásához hozzájárul. Az ilyen gyülekezeti tagnak nagykorúsága elérésétől kezdődően választó joga van és választható az egyházi vagy gyülekezeti választások alkalmával.
                </p>
            <h3> Lehetőségek</h3>
                <p>
                    Az egyháztagság nem feltétele annak, hogy valaki részt vehessen istentiszteleti alkalmakon. Ehhez még az sem szükséges, hogy meg legyen keresztelve. Ha viszont valaki szeretne megkeresztelkedni vagy konfirmálni (hitét megvallva teljes jogú, úrvacsorával élő egyháztaggá válni), szívesen és örömmel részesítjük keresztelési illetve konfirmációi felkészítésben. Gyülekezetünk alkalmai nyitottak, és mindenkit szeretettel várunk!
                </p>
            <h3>Egyházfenntartói Járulék</h3>
                <p>
                    A gyülekezetek önfenntartóak. A közhiedelemmel ellentétben ugyanis sem az államtól sem az egyháztól nem kapnak pénzt a működésükhöz. Az egyháztagok által fizetett egyházfenntartói járulék fedezi többek között a személyi kiadásokat (egyházi és világi alkalmazottak munkabére és járulékai), a gyülekezeti élettel kapcsolatos költségeket (konfirmáció, hitoktatás, missziói alkalmak, szeretetszolgálat, stb.), az igazgatási kiadásokat (posta, telefon, irodaszerek, gyülekezeti könyvtár, stb.), a gyülekezeti épületeink fenntartását és felújítását (templom, parókia, gyülekezeti ház, beosztott lelkipásztori lakás).
                </p>
                <p>
                    Az egyházfenntartást a Magyarországi Református Egyház törvénye szabályozza, és minden lelkipásztornak, presbiternek és egyháztagnak ezt kell szem előtt tartania. Ez kimondja: “Az egyháztag hitből fakadó kötelessége, hogy egyházfenntartói járulékot fizessen, amelynek mértékét a gyülekezet presbitériuma határozza meg – azonban nem lehet kevesebb, mint az egyháztag jövedelmének, nyugdíjának legalább 1%-a.”
                </p>
                <p> Presbitériumunk a 2020. évi egyházfenntartás minimális összegét </p>
                    <ul>
                        <li> aktív keresőknek 16.000 Ft </li>
                        <li> nyugdíjasoknak 8.000 Ft </li>
                        <li> diákoknak, GYES-en lévőknek 4.000 Ft </li>
                    </ul>
                <p> ajánlott éves összegben határozta meg. </p>
                <p> Az egyházfenntartói járulékot be lehet fizetni </p>
                    <ul>
                        <li> személyesen a lelkipásztori hivatalban, a körzetgazdáknál, illetve a vasárnapi istentisztelet után a templomban, </li>
                        <li> csekken, </li>
                        <li> folyószámlára átutalással </li>
                    </ul>
                <p> Bankszámla szám: 10918001-00000061-89060000. (Unicredit Bank) </p>
                <p>
                    Javasoljuk, hogy ha tehetik, havonta, rendszeres átutalással fizessék be az egyházfenntartói járulék részleteit annak érdekében, hogy az egyházközség folyamatosan tudja a kiadásokat finanszírozni.
                </p>
                 <p> Adó 1% technikai szám: 0066 </p>
                 <p> Ezúton is köszönjük mindenkinek az adományait! </p>
        </div>
