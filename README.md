# pecelref

A new website for the Reformed Church Community of Pécel, Hungary, using plain HTML, PHP, CSS and MySQL technologies, with maybe a bit of JavaScript for responsiveness.

Also a homework assignment for Informatics 2 at Budapest University of Technology and Economics, Faculty of Electrical Engineering and Informatics.

To be deployed to a real web server, currently living inside a test environment
