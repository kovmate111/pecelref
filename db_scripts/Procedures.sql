DELIMITER //;

-------------------
-- Bejelentkezés --
-------------------

-- TODO login

---------------------
-- Bibliai könyvek --
---------------------

-- Fejezetek egy könyvben
DELETE PROCEDURE IF EXISTS pecelref.fejezetek_konyvben//
CREATE PROCEDURE pecelref.fejezetek_konyvben (IN konyvId INT)
BEGIN
    SELECT COUNT(bib_fejezet.id) AS fejezetszam
    FROM pecelref.bib_fejezet
    WHERE bib_fejezet.konyvId = konyvId;
END//

-- Versek egy fejezetben
DELETE PROCEDURE IF EXISTS pecelref.versek_fejezetben//
CREATE PROCEDURE pecelref.versek_fejezetben (IN fejezetId INT)
BEGIN
    SELECT COUNT(igehely.id) AS versszam
    FROM pecelref.igehely
    WHERE igehely.fejezetid = fejezetId;
END//

-- Könyvek listázása legördülő menühöz
DELETE PROCEDURE IF EXISTS pecelref.bib_konyvek//
CREATE PROCEDURE pecelref.bib_konyvek ()
BEGIN
    SELECT cim
    FROM pecelref.bib_konyv
    ORDER BY id ASC;
END//

-- Igehely szövege
DELETE PROCEDURE IF EXISTS pecelref.igehely_szoveg//
CREATE PROCEDURE pecelref.igehely_szoveg (IN igehelyId INT)
BEGIN
    SELECT igehely.versszam, igehely.szoveg, bib_fejezet.fejezetszam, bib_konyv.rovidites
    FROM pecelref.igehely
        INNER JOIN pecelref.bib_fejezet ON igehely.fejezetId = bib_fejezet.id
        INNER JOIN pecelref.bib_konyv ON bib_fejezet.konyvId = bib_konyv.id
    WHERE igehely.id = igehelyId;
END//

---------------
-- Navigáció --
---------------

-- Átirányítás kulcsszó alapján
DELETE PROCEDURE IF EXISTS pecelref.keres_oldalt//
CREATE PROCEDURE pecelref.keres_oldalt (IN kulcsszo NVARCHAR(256))
BEGIN
    SELECT oldal.*
    FROM pecelref.oldal
    WHERE oldal.keywords LIKE "*," + kulcsszo + ",*"
        OR oldal.keywords LIKE kulcsszo + ",*"
        OR oldal.keywords LIKE "*," + kulcsszo
    LIMIT 1;
END

-- Átirányítás id alapján
DELETE PROCEDURE IF EXISTS pecelref.meghiv_oldalt//
CREATE PROCEDURE pecelref.meghiv_oldalt (IN oldalId INT)
BEGIN
    SELECT oldal.*
    FROM pecelref.oldal
    WHERE oldal.id = oldalId;
END//

-- Főoldalak címének listázása menühöz
DELETE PROCEDURE IF EXISTS pecelref.menu_felsoszint_oldalak//
CREATE PROCEDURE pecelref.menu_felsoszint_oldalak ()
BEGIN
    SELECT oldal.bannerTitle, oldal.numberInMenu, oldal.id
    FROM pecelref.oldal
    WHERE oldal.csoportId IS NULL,
    ORDER BY numberInMenu ASC;
END//

-- Menü kategóriák címének listázása
DELETE PROCEDURE IF EXISTS pecelref.menu_felsoszint_almenuk//
CREATE PROCEDURE pecelref.menu_felsoszint_almenuk ()
BEGIN
    SELECT oldalCsoport.titleInMenu, oldalCsoport.numberInMenu, oldalCsoport.id
    FROM pecelref.oldalCsoport
    ORDER BY numberInMenu ASC;
END//
-- Ezek összefűzésével lehet a felső sort elkészíteni, de az almenükhöz több kell. A jelenlegi oldalt szűkíteni kell.

-- Adott almenü listázása
DELETE PROCEDURE IF EXISTS pecelref.menu_almenu//
CREATE PROCEDURE pecelref.menu_almenu (IN csoportId INT)
BEGIN
    SELECT oldal.bannerTitle, oldal.numberInMenu, oldal.id
    FROM pecelref.oldal
    WHERE oldal.csoportId = csoportId
    ORDER BY oldal.numberInMenu ASC;
END//

-------------------
-- Heti alkalmak --
-------------------

-- Naponként a Rendszeres Alkalmak oldalhoz:
CREATE PROCEDURE pecelref.rendszeres_nap (IN hetnap INT)
BEGIN
    SELECT rendszeres_szolgalat.megnevezes AS megnevezes, hetNapja, TIME_FORMAT(ido, "%H:%i"), helyszin.megnevezes AS helyszin, rendszeresseg --TODO
    FROM pecelref.hetiAlkalmak
        INNER JOIN pecelref.helyszin ON hetiAlkalmak.helyszinId = helyszin.id
    WHERE hetNapja = hetnap
    ORDER BY ido ASC;
END//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'editor_php'@'localhost'//

-----------
-- Hírek --
-----------

-- Legutolsó 6 a Főoldalhoz
CREATE PROCEDURE pecelref.hirek_fooldal ()
BEGIN
    SELECT hirbejegyzes.id, hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum
    FROM pecelref.hirbejegyzes
    ORDER BY datum DESC
    LIMIT 6;
END//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqHome TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqHome TO 'editor_php'@'localhost'//

-- Előző 6 hónap a Hírek oldalhoz:
CREATE PROCEDURE pecelref.hirek ()
BEGIN
    SELECT hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username
    FROM pecelref.hirbejegyzes
        INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id
    WHERE MONTH(curdate())-MONTH(datum) <= 6
    ORDER BY datum DESC;
END//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'editor_php'@'localhost'//

-- Év alapján, a Hírek oldalhoz:
CREATE PROCEDURE pecelref.hirek_year (IN yr INT)
BEGIN
    SELECT hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username
    FROM pecelref.hirbejegyzes
        INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id
    WHERE YEAR(datum) = yr
    ORDER BY datum DESC;
END//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'editor_php'@'localhost'//

-- Keresés szerint, a Hírek oldalhoz:
CREATE PROCEDURE pecelref.hirek_matchtxt (IN kulcsszo NVARCHAR(120))
BEGIN
    SELECT hirbejegyzes.cim, hirbejegyzes.szoveg, hirbejegyzes.datum, felhasznalok.username
    FROM pecelref.hirbejegyzes
        INNER JOIN pecelref.felhasznalok ON hirbejegyzes.felhasznaloId=felhasznalok.id
    WHERE cim LIKE CONCAT('%', @kulcsszo, '%') OR szoveg LIKE CONCAT('%', @kulcsszo, '%')
    ORDER BY datum DESC;
END//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.hirekRqAll TO 'editor_php'@'localhost'//

-- Bejelentkeztetés

CREATE PROCEDURE pecelref.login_proc (IN uname VARCHAR(20), IN qHash VARCHAR(128), OUT userExists BOOLEAN, OUT pwdOK BOOLEAN)
BEGIN
    IF NOT EXISTS (
        SELECT username
        FROM pecelref.felhasznalok
        WHERE username LIKE uname
    ) THEN
        SET userExists FALSE;
    ELSE
        SET userExists TRUE;
        IF NOT EXISTS (
            SELECT username, saltedPwdHash
            FROM pecelref.felhasznalok
            WHERE username LIKE uname AND saltedPwdHash LIKE qHash
        ) THEN
            SET pwdOK FALSE;
        ELSE SET pwdOK TRUE;
        END IF;
    END IF;
END//
GRANT EXECUTE ON PROCEDURE pecelref.loginRq TO 'visitor_php'@'localhost'//
GRANT EXECUTE ON PROCEDURE pecelref.loginRq TO 'editor_php'@'localhost'//

-- Legutolsó N igehirdetés listázása

CREATE PROCEDURE pecelref.ihRqAlap (IN dbsz INT)
BEGIN
    SELECT igehirdetes.cim, igehirdetes.datum, igehirdetes.youtubeURL, igehirdetes.hangURL, 
